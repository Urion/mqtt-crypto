[![Angular Logo](./logo-angular.jpg)](https://angular.io/) [![Electron Logo](./logo-electron.jpg)](https://electron.atom.io/)

# MQTT Crypto

![MQTT Cripto](./src/favicon.256x256.png)

# Getting Started

Clone this repository locally :

``` bash
git clone https://gitlab.com/Urion/mqtt-crypto
```

Install dependencies with npm :

``` bash
npm install
```
Run the application :

``` bash
npm start
```

Build the application :

- Linux 
``` bash
npm run electron:linux
```

- Windows
``` bash 
npm run electron:windows
```

- Mac
``` bash
npm run electron:mac
```
