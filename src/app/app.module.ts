import 'zone.js/dist/zone-mix'
import 'reflect-metadata'
import '../polyfills'
import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

// Forms modules
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

// Material modules
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { MatButtonModule } from '@angular/material/button'
import { MatIconModule } from '@angular/material/icon'
import { MatTooltipModule } from '@angular/material/tooltip'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatGridListModule } from '@angular/material/grid-list'
import { MatSnackBarModule } from '@angular/material/snack-bar'
import { MatCardModule } from '@angular/material/card'
import { MatInputModule } from '@angular/material/input'
import { MatFormFieldModule } from '@angular/material/form-field'
import { MatDialogModule } from '@angular/material/dialog'
import { MatSelectModule } from '@angular/material/select'
import { MatListModule } from '@angular/material/list'

const MATERIAL_MODULES = [
  BrowserAnimationsModule,
  MatButtonModule,
  MatIconModule,
  MatTooltipModule,
  MatToolbarModule,
  MatGridListModule,
  MatSnackBarModule,
  MatCardModule,
  MatInputModule,
  MatFormFieldModule,
  MatDialogModule,
  MatSelectModule,
  MatListModule
]

// Scrollbar
import { PerfectScrollbarModule, PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar'
import { InfiniteScrollModule } from 'ngx-infinite-scroll'

import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to'

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
}

// Toastr modules
// https://www.npmjs.com/package/ngx-toastr - to configure
import { ToastrModule } from 'ngx-toastr'

// Http client modules
import { HttpClientModule, HttpClient } from '@angular/common/http'

// Routing modules
import { AppRoutingModule } from './app-routing.module'

// Angular translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core'
import { TranslateHttpLoader } from '@ngx-translate/http-loader'

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json')
}

// Electron services
import { ElectronService } from './providers/electron.service'

// App components
import { AppComponent } from './app.component'
import { HomeComponent } from './components/home/home.component'
import { ConfigurationDialogComponent } from './components/configuration-dialog/configuration-dialog.component'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ConfigurationDialogComponent
  ],
  imports: [
    BrowserModule,
    MATERIAL_MODULES,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    PerfectScrollbarModule,
    InfiniteScrollModule,
    ScrollToModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 3000,
      autoDismiss: true,
      preventDuplicates: true,
      resetTimeoutOnDuplicate: true,
      maxOpened: 5,
      newestOnTop: true
    })
  ],
  providers: [
    ElectronService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [ConfigurationDialogComponent]
})
export class AppModule { }
