import { Component, OnInit, Inject } from '@angular/core'
import { BrokerConfiguration } from '../../models/broker-configuration'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'

@Component({
  selector: 'app-configuration-dialog',
  templateUrl: './configuration-dialog.component.html',
  styleUrls: ['./configuration-dialog.component.scss']
})
export class ConfigurationDialogComponent implements OnInit {

  public configuration = {
    brokerAddress: '',
    brokerPort: 1883,
    protocol: '',
    clientId: '',
    username: '',
    password: '',
    qos: 0
  }

  public protocols = [
    'wss',
    'ws',
    'mqtt',
    'mqtts',
    'tcp',
    'ssl',
    'wx',
    'wxs'
  ]

  public QoS = [
    0,
    1,
    2
  ]

  constructor(public dialogRef: MatDialogRef<ConfigurationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: BrokerConfiguration) { }

  ngOnInit(): void {
    this.configuration.brokerAddress = this.data.getBrokerAddress()
    this.configuration.brokerPort    = this.data.getBrokerPort()
    this.configuration.protocol      = this.data.getProtocol()
    this.configuration.clientId      = this.data.getClientId()
    this.configuration.username      = this.data.getUsername()
    this.configuration.password      = this.data.getPassword()
    this.configuration.qos           = this.data.getQos()
  }

}
