import { Component, OnInit, ChangeDetectorRef } from '@angular/core'
import { BrokerConfiguration } from '../../models/broker-configuration'
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar'
import connectClient from '../../utils/mqtt-connection'
import { Root } from 'protobufjs'
import { Topic } from '../../models/topic'
import { ConfigurationDialogComponent } from '../configuration-dialog/configuration-dialog.component'
import { MatDialog } from '@angular/material/dialog'
import { ToastrService } from 'ngx-toastr'
import { ShowedMessage } from '../../models/showed-message'
import { config } from 'rxjs'
import { FormGroup, FormBuilder } from '@angular/forms'
import { ProtoInfo } from '../../models/protoInfo'
import { Messages } from '../../models/messages'
import { basename } from 'path'
import { forEach } from '@angular/router/src/utils/collection';

const storage     = require('electron-storage')
const storagePath = 'data/'

const {dialog} = require('electron').remote

let client

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public brokerConf: BrokerConfiguration = new BrokerConfiguration('', 1883, 'mqtt', 'MQTT_Client_ID', '', '', 0)
  public topics: Topic[] = [
    new Topic('')
  ]

  public messages: Array<Messages> = new Array()
  public focussedMessage = new ShowedMessage('', '')

  public mqttFile    = 'mqttConfig.json'
  public topicFile   = 'topicConfig.json'
  public protobuf    = 'protobuf.json'

  public currentIndex: number

  public recievedMessages = 0

  public protoForm: FormGroup
  public publishForm: FormGroup

  public proto: ProtoInfo = new ProtoInfo('', '', '', '')

  public publishingTopic: string
  public publishingMessage: string
  public publishingQos: number

  constructor(
    private _formBuilder: FormBuilder,
    public toastr: ToastrService,
    public matDialog: MatDialog,
    private cdr: ChangeDetectorRef
  ) { }

  /**
   * Show a success toastr
   * @param title The title of the toastr
   * @param message The content of the toastr
   */
  showSuccess(title?: string, message?: string) {
    this.toastr.success(message, title, {
      closeButton: true,
      onActivateTick: true
    })
  }

  /**
   * Show a warn toastr
   * @param title The title of the toastr
   * @param message The content of the toastr
   */
  showWarn(title?: string, message?: string) {
    this.toastr.warning(message, title, {
      closeButton: true,
      onActivateTick: true
    })
  }

  /**
   * Show a info toastr
   * @param title The title of the toastr
   * @param message The content of the toastr
   */
  showInfo(title?: string, message?: string) {
    this.toastr.info(message, title, {
      closeButton: true,
      onActivateTick: true
    })
  }

  /**
   * Show a error toastr
   * @param title The title of the toastr
   * @param message The content of the toastr
   */
  showError(title?: string, message?: string) {
    this.toastr.error(message, title, {
      closeButton: true,
      onActivateTick: true
    })
  }

  /**
   * Saves configuration into a json file. If wanna save topics, it automatically removes all the empty value
   * @param configToSave Configuration object to save
   * @param fileName Name of the file to save
   */
  saveConfig(configToSave, fileName) {
    console.log(configToSave)
    storage.remove(`${storagePath}${fileName}`)
      .then( err => {
        if (err) {
          console.error(err)
        } else {
          if (fileName === this.topicFile) {
            for (let i = configToSave.length; i > 0; i --) {
              if (configToSave[i - 1].value === '') {
                configToSave.splice(i - 1, 1)
              }
            }
          }
          storage.set(`${storagePath}${fileName}`, configToSave, error => {
            if (error) {
              console.error(error)
              this.showWarn('Configuration Storage', 'Can\'t save your changes.')
            } else {
              console.log(`Saved changes on ${storagePath}${fileName}`)
              this.showSuccess('Configuration Storage', 'Configuration succesfully saved.')
            }
          })
        }
      })
  }

  /**
   * Delete a json file where configuration are stored
   * @param fileName Name of the file to delete
   */
  deleteConfig(fileName) {
    storage.remove(`${storagePath}${fileName}`).then( err => {
      if (err) {
        console.error(err)
        this.showWarn('Configuration Storage', 'Can\'t delete configuration.')
      } else {
        this.showSuccess('Configuration Storage', 'Configuration succesfully deleted.')
      }
    })
  }

  /**
   * Connects to the mqtt broker
   */
  connectBroker() {
    const self = this

    if (client) {
      if (client.connected) {
        self.showWarn('Broker Connection', 'You are already connected to a broker')
        return
      }
    }

    client = connectClient(
      this.brokerConf.getBrokerAddress(),
      this.brokerConf.getBrokerPort(),
      this.brokerConf.getProtocol(),
      this.brokerConf.getUsername(),
      this.brokerConf.getPassword(),
      this.brokerConf.getQos()
    )

    client.on('connect', function() {
      self.showSuccess('Broker Connection', 'Broker connection estabilshed.')
    })
    client.on('error', function() {
      self.showWarn('Broker Connection', 'Cannot connect to broker server.')
    })
    client.on('offline', function() {
      self.showWarn('Broker Connection', 'Cannot connect to broker server.')
    })
    client.on('end', function() {
      self.showSuccess('Broker Connection', 'Disconnected from broker server.')
    })
  }

  /**
   * Disconnect from the broker
   */
  disconnectBroker() {
    if (client) {
      if (client.connected) {
        client.end()
      } else {
        this.showWarn(`Broker Connection`, `You are not connected to any broker`)
      }
    } else {
      this.showWarn(`Broker Connection`, `You are not connected to any broker`)
    }
  }

  /**
   * Add a new topic to the list (MAX 6 topics)
   */
  addNewTopic() {
    if (this.topics.length < 6) {
      this.topics.push(new Topic(''))
    } else {
      this.showWarn('Topics', 'Too much topics. Remove another one before')
    }
  }

  /**
   * Remove a topic at specific index (MIN 1 topic)
   * @param index Element to remove
   */
  removeATopic(index) {
    if (this.topics.length > 1) {
      this.topics.splice(index, 1)
    }
  }

  /**
   * Open mqtt broker configuration dialog
   */
  openDialog(): void {
    const dialogRef = this.matDialog.open(ConfigurationDialogComponent, {
      width: '80%',
      data: this.brokerConf
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.brokerConf.setBrokerAddress(result.brokerAddress ? result.brokerAddress : '')
        this.brokerConf.setBrokerPort(result.brokerPort ? result.brokerPort : 1883)
        this.brokerConf.setProtocol(result.protocol ? result.protocol : '')
        this.brokerConf.setClientId(result.clientId ? result.clientId : '')
        this.brokerConf.setUsername(result.username ? result.username : '')
        this.brokerConf.setPassword(result.password ? result.password : '')
        this.brokerConf.setQos(result.qos ? result.qos : 0)
        console.log(this.brokerConf)
        this.saveConfig(this.brokerConf, this.mqttFile)
      }
    })
  }

  /**
   * Subscribe to an array of topic
   */
  subscribeTopic() {
    const self = this
    for (let i = this.topics.length; i > 0; i --) {
      if (this.topics[i - 1].value === '') {
        this.topics.splice(i - 1, 1)
      }
    }
    if (client) {
      if (client.connected) {
        this.topics.forEach( topic => {
          if (client === undefined) {
            self.showWarn(`Client not connected.`, `Please connect to a broker before subscribe`)
            return
          }
          self.showSuccess(`Subscribed to topic ${topic.value}.`)
          client.subscribe(topic.value)
          .on('message', function (topic, message) {
            self.recievedMessages += 1
            const mess: Messages = new Messages(message, topic.toString())
            if (self.messages.length >= 500) {
              self.messages.splice(0, 1)
            }
            self.messages = [...self.messages, mess]
            self.cdr.detectChanges()
            const objDiv = document.getElementById('topic-list')
            objDiv.scrollTop = objDiv.scrollHeight
          })
        })
      } else {
        self.showWarn(`Client not connected.`, `Please connect to a broker before subscribe`)
      }
    } else {
      self.showWarn(`Client not connected.`, `Please connect to a broker before subscribe`)
    }
  }

  /**
   * Unsubscribe from an array of topic
   */
  unsubscribeTopic() {
    const self = this
    if (client) {
      if (client.connected) {
        if (client === undefined) {
          self.showWarn(`Client not connected.`, `You are not connected to a broker`)
          return
        }
        self.topics.forEach( topic => {
          client.unsubscribe(topic.value)
        })
        self.clearBuffer()
        console.log('Succesfully unsubscribed')
      } else {
        self.showWarn(`Client not connected.`, `You are not connected to a broker`)
      }
    } else {
      self.showWarn(`Client not connected.`, `You are not connected to a broker`)
    }

  }

  /**
   * Open current message
   * @param index the index of the message
   */
  openMessage( index: number) {
    this.currentIndex = index
    this.toHex()
    console.log('Current message: ', this.focussedMessage.message)
    this.focussedMessage.topic = this.messages[index].topic
  }

  /**
   * Convert the message to Hex
   */
  toHex() {
    const self = this
    if ( self.messages[self.currentIndex] !== undefined ) {
      if ( self.messages[self.currentIndex].message.toString() ) {
        let result = ''
        for (let i = 0; i < self.messages[self.currentIndex].message.toString().length; i++) {
          result  += '000' + self.messages[ self.currentIndex].message[i].toString(16).slice(-4) + ' '
          // if (i % 32 === 0 && i !== 0) {
          //   result += '\n'
          // }
        }
        self.focussedMessage.message = result.toUpperCase()
        const preTag = document.getElementById('focussed-message')
        preTag.style.setProperty('white-space', 'normal')
        self.showSuccess('Conversion', 'Message converted to HEX.')
      } else {
        self.showWarn('Conversion', 'Select a message before.')
      }
    } else {
      self.showWarn('Conversion', 'Select a message before.')
    }
  }

  /**
   * Converts message to json
   */
  toJson() {
    const self = this
    const pbRoot = new Root()
    const isMessageSelected =
      self.messages[self.currentIndex] !== undefined &&
      self.messages[self.currentIndex].message.toString()


    if (!isMessageSelected) {
      self.showWarn('Conversion', 'Select a message before.')

      return
    }
    pbRoot.resolvePath = (origin: string, target: string) => {
      const result = self.proto.protobufPath + '/' + target
      return result
    }

    pbRoot.load(this.proto.protobufFile, { keepCase: true }, function(err, root) {
      if (err) {
        self.showWarn('Conversion', 'Failed to convert message to JSON.')
        console.log(err)
        throw err
      } else {
        const myMessage = root.lookupType(self.proto.protobufPackage + '.' + self.proto.protobufMessage)
        const array: Uint8Array = self.messages[self.currentIndex].message.slice()
        const error = myMessage.verify(myMessage.decode(array))
        if (error) {
          self.showWarn('Conversion', 'Cannot convert this message.')
        } else {
          self.focussedMessage.message = JSON.stringify(myMessage.decode(array), null, '\t')
          const preTag = document.getElementById('focussed-message')
          preTag.style.setProperty('white-space', 'pre-wrap')
          self.showSuccess('Conversion', 'Message converted to JSON.')
        }
      }
    })
  }

  /**
   * Converts message to string
   */
  toStrings() {
    const self = this
    if ( self.messages[self.currentIndex] !== undefined ) {
      if ( self.messages[self.currentIndex].message.toString() ) {
        self.focussedMessage.message = self.messages[self.currentIndex].message.toString()
        const preTag = document.getElementById('focussed-message')
        preTag.style.setProperty('white-space', 'normal')
        self.showSuccess('Conversion', 'Message converted to STRING.')
      } else {
        self.showWarn('Conversion', 'Failed to convert message to String.')
      }
    } else {
      self.showWarn('Conversion', 'Select a message before.')
    }
  }

  /**
   * Copy some text
   * @param text text to copy (the message)
   */
  copyMessage(text: string) {
    const textArea = document.createElement('textarea')
    textArea.style.position = 'fixed';
    textArea.style.top = '0';
    textArea.style.left = '0';
    textArea.style.width = '2em';
    textArea.style.height = '2em';
    textArea.style.padding = '0';
    textArea.style.border = 'none';
    textArea.style.outline = 'none';
    textArea.style.boxShadow = 'none';
    textArea.style.background = 'transparent';
    textArea.value = text;
    document.body.appendChild(textArea);
    textArea.select();
    try {
      const successful = document.execCommand('copy');
      const msg = successful ? 'successful' : 'unsuccessful';
      console.log('Copying text command was ' + msg);
      this.showSuccess('Copy', 'Message copied on clipboard ' + msg)
    } catch (err) {
      this.showWarn('Copy', 'Oops, unable to copy')
      console.log('Oops, unable to copy');
    }
    document.body.removeChild(textArea);
  }

  /**
   * Removes all the message from the array messages
   */
  clearBuffer() {
    const self = this
    if ( client.connected ) {
      self.messages = new Array()
      self.cdr.detectChanges()
      self.showSuccess('Clean messages', 'Removed all the messages')
    } else {
      self.showWarn('Clean messages', 'No message to remove')
    }
  }

  /**
   * Open file manager to select protobuf file
   */
  openFile() {

    const pathToFile = dialog.showOpenDialog({properties: ['openFile']})[0]

    this.proto.protobufFile = basename(pathToFile)

    this.proto.protobufPath = pathToFile.replace(basename(pathToFile), '')

  }

  /**
   * Clear selected message
   */
  clearMessage() {
    this.focussedMessage = new ShowedMessage('', '')
  }

  /**
   * Clear textarea
   */
  cleanTextArea() {
    const elem = document.getElementById('publish-text-area') as HTMLInputElement;
    elem.value = ''
  }

  /**
   * Convert hex to string
   * @param hex the hex to convert
   */
  fromHexToString(hex) {

    let result

    for (let j = 0; j < hex.length; j++) {
        result += String.fromCharCode(parseInt(hex[j], 16))
    }

    return result
  }

  /**
   * Publish message as string
   */
  publishString() {
    client.publish(this.publishingTopic, this.publishingMessage, this.publishingQos ? this.publishingQos : '0')
    this.showSuccess('Publish message', 'Message published on MQTT.')
  }

  /**
   * Publish message as Hex
   */
  publishHex() {
    client.publish(this.publishingTopic, this.fromHexToString(this.publishingMessage), this.publishingQos ? this.publishingQos : '0')
    this.showSuccess('Publish message', 'Message published on MQTT.')
  }

  /**
   * Publish message as protobuf message
   */
  publish() {
    const self = this
    const pbRoot = new Root()
    pbRoot.resolvePath = (origin: string, target: string) => {
      const result = self.proto.protobufPath + '/' + target
      return result
    }

    pbRoot.load(this.proto.protobufFile, { keepCase: true }, function(err, root) {
      if (err) {
        console.log(err)
        self.showWarn('Publish message', 'Failed to convert message to JSON.')
        throw err
      } else {
        const myMessage = root.lookupType(self.proto.protobufPackage + '.' + self.proto.protobufMessage)
        const payload = myMessage.create(JSON.parse(self.publishingMessage))
        const str = myMessage.encode(payload).finish()
        console.log(str.join(' ,'))
        client.publish(self.publishingTopic, str, self.publishingQos ? self.publishingQos : '0')
        self.showSuccess('Publish message', 'Message published on MQTT.')
      }
    })
  }

  ngOnInit() {
    storage.isPathExists(`${storagePath}${this.mqttFile}`)
      .then(itDoes => {
        if (itDoes) {
          storage.get(`${storagePath}${this.mqttFile}`)
            .then(data => {
              this.brokerConf = new BrokerConfiguration(
                data.brokerAddress,
                data.brokerPort,
                data.protocol,
                data.clientId,
                data.username,
                data.password,
                data.qos
              )
              this.cdr.detectChanges()
              console.log(this.brokerConf)
            })
            .catch(err => {
              console.error(err)
            })
        }
    })

    storage.isPathExists(`${storagePath}${this.topicFile}`)
      .then(itDoes => {
        if (itDoes) {
          storage.get(`${storagePath}${this.topicFile}`)
            .then(data => {
              console.log(`Saved topics are ${data}`)
              this.topics = data
              this.cdr.detectChanges()
              console.log(this.topics)
            })
            .catch(err => {
              console.error(err)
            })
        }
    })

    this.protoForm = this._formBuilder.group({
      protobufPath: this._formBuilder.control,
      protobufFile: this._formBuilder.control,
      protoPackage: this._formBuilder.control,
      protoMessage: this._formBuilder.control
    })

    this.publishForm = this._formBuilder.group({
      publishProtoPath: this._formBuilder.control,
      publishProtoFile: this._formBuilder.control,
      publishProtoPack: this._formBuilder.control,
      publishProtoMess: this._formBuilder.control
    })

    storage.isPathExists(`${storagePath}${this.protobuf}`)
    .then(itDoes => {
      if (itDoes) {
        storage.get(`${storagePath}${this.protobuf}`)
          .then(data => {
            this.protoForm.setValue({
              protobufPath: data.protobufPath,
              protobufFile: data.protobufFile,
              protoPackage: data.protoPackage,
              protoMessage: data.protoMessage
            })
            this.cdr.detectChanges()
          })
          .catch(err => {
            console.error(err)
          })
      }
    })
  }

}
