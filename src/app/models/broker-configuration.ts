export class BrokerConfiguration {
  private brokerAddress:  string
  private brokerPort:     number
  private protocol:       'wss' | 'ws' | 'mqtt' | 'mqtts' | 'tcp' | 'ssl' | 'wx' | 'wxs'
  private clientId:       string
  private username:       string
  private password:       string
  private qos:            number

  constructor(
    brokerAddress:  string,
    brokerPort:     number,
    protocol:       'wss' | 'ws' | 'mqtt' | 'mqtts' | 'tcp' | 'ssl' | 'wx' | 'wxs',
    clientId:       string,
    username:       string,
    password:       string,
    qos:            number,
  ) {
    this.brokerAddress  = brokerAddress
    this.brokerPort     = brokerPort
    this.protocol       = protocol
    this.clientId       = clientId
    this.username       = username
    this.password       = password
    this.qos            = qos
  }

  // Broker address
  getBrokerAddress(): string {
    return this.brokerAddress
  }

  setBrokerAddress(brokerAddress: string): void {
    this.brokerAddress = brokerAddress
  }

  // Broker port
  getBrokerPort(): number {
    return this.brokerPort
  }

  setBrokerPort(brokerPort: number): void {
    this.brokerPort = brokerPort
  }

  // Protocol
  getProtocol(): string {
    return this.protocol
  }

  setProtocol(protocol: 'wss' | 'ws' | 'mqtt' | 'mqtts' | 'tcp' | 'ssl' | 'wx' | 'wxs'): void {
    this.protocol = protocol
  }

  // Client ID
  getClientId(): string {
    return this.clientId
  }

  setClientId(clientId: string): void {
    this.clientId = clientId
  }

  // Username
  getUsername(): string {
    return this.username
  }

  setUsername(username: string): void {
    this.username = username
  }

  // Password
  getPassword(): string {
    return this.password
  }

  setPassword(password: string): void {
    this.password = password
  }

  // QoS
  getQos(): number {
    return this.qos
  }

  setQos(qos: number): void {
    this.qos = qos
  }

}
