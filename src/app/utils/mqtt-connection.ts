import { connect } from 'mqtt'

/**
 * Connect to an mqtt broker
 * @param brokerAddress Server where mqtt broker is hosted
 * @param brokerPort Port of mqtt broker
 * @param protocol Protocol of mqtt connection
 * @param brokerUser Username for broker authentication
 * @param brokerPassword Password for broker authentication
 * @param qos QOS type
 */
export default function connectClient(brokerAddress, brokerPort, protocol, brokerUser, brokerPassword, qos) {
    const client = connect({
        host: brokerAddress,
        port: brokerPort,
        protocol: protocol,
        username: brokerUser,
        password: brokerPassword,
        qos: qos
    })

    client.on('connect', () => {
        console.log('connected')
    })

    return client
}
